from django.shortcuts import render, redirect
from .forms import formLandingpage
from .models import landing_pageDatabase

# Create your views here.
def landingpage(request):
    if request.method == 'POST' : 
        form = formLandingpage(request.POST)
        if form.is_valid() :
            form.save()
            return redirect('landingpage')
    else:
        form = formLandingpage()
    isi_status = landing_pageDatabase.objects.all()
    response = {
       'form' : form,
        'isi_status' : isi_status,
        }
    return render(request, 'page.html', response)