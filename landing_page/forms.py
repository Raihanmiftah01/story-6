from django import forms
from .models import landing_pageDatabase

class formLandingpage(forms.ModelForm) :
    class Meta :
        model = landing_pageDatabase
        fields = ['status']