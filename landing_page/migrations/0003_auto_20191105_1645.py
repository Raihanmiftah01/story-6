# Generated by Django 2.2.5 on 2019-11-05 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing_page', '0002_auto_20191105_1635'),
    ]

    operations = [
        migrations.CreateModel(
            name='landing_pageDatabase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(max_length=300)),
                ('waktu', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Message',
        ),
    ]
