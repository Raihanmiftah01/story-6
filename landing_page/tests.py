from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from django.utils import timezone
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import landingpage
from .models import landing_pageDatabase
from .forms import formLandingpage

# Create your tests here.

class story6UnitTest(TestCase):

    def test_ada_url_ga(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_tulisan_halo_apa_kabar(self):
        response = Client().get('')
        self.assertContains(response, 'Halo, apa kabar?')
        self.assertEqual(response.status_code, 200)

    def test_html_nya(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'page.html')

    def test_button_update(self):
        a = Client()
        response = a.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content)

    def test_create_new_model(self):
        new_message = landing_pageDatabase.objects.create(waktu=timezone.now(), status="Alhamdulillah")
        count_pesan = landing_pageDatabase.objects.all().count()

        self.assertEqual(count_pesan, 1)

class story6FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(25)
        super(story6FunctionalTest,self).setUp()

        
    def tearDown(self):
        self.browser.quit()
        super(story6FunctionalTest, self).tearDown()
    
    def test_post(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Landing Page', self.browser.title)
        inputstatus = self.browser.find_element_by_id('id_status')
        time.sleep(1)
        inputstatus.send_keys('Coba coba')
        time.sleep(1)
        inputstatus.send_keys(Keys.ENTER)
        time.sleep(5)
        self.assertIn('Coba coba', self.browser.page_source)
